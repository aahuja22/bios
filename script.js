// For Field Force 1st Tab  

document.getElementById("mybtnFF").addEventListener("click",function(event){
    // console.log(event);
    // console.log(event.target);
    // console.log(event.target.style.display);
    var a =document.getElementsByClassName("click_me");

 // for opening one at a time FF or MA or SOT etc or others
    Array.from(a).forEach(element => {
        var img_icon = "img_icon"+ element.id.slice(-(element.id.length-5)); 
        var Ltab_header = "Ltab_header" + element.id.slice(-(element.id.length-5));
        document.getElementById(Ltab_header).style.transform = "translate(0,0)" ;
        document.getElementById(img_icon).style.margin = "0 0 0 0";

        element.style.display = "none";
    });
//opening 1 tab out of 4 ends

    var box=document.getElementById("mydivFF");

    // console.log(box.style.display);

    if(box.style.display == "none")
    {
        box.style.display = "block" ;
        document.getElementById("img_iconFF").style.margin = "30px 0 0 0";
        document.getElementById("Ltab_headerFF").style.transform = "translate(0,45%)";
        // document.getElementById("caretFF").style.marginTop = "17px";
       
    }
    else
    {
        document.getElementById("img_iconFF").style.margin = "0 0 0 0";
        document.getElementById("Ltab_headerFF").style.transform = "translate(0,0)";
        // document.getElementById("caretFF").style.marginTop = "0";
        box.style.display = "none";
    }

});


// 2nd TAB starts MA

document.getElementById("mybtnMA").addEventListener("click",function(event){
  
    var a =document.getElementsByClassName("click_me");

    Array.from(a).forEach(element => {
        var img_icon = "img_icon"+ element.id.slice(-(element.id.length-5));
        var Ltab_header = "Ltab_header" + element.id.slice(-(element.id.length-5));
        // console.log(Ltab_header)
        // var Ltab_caret = "caret" + element.id.slice(-(element.id.length-5));
        // document.getElementById(Ltab_caret).style.marginTop = "0";
        document.getElementById(Ltab_header).style.transform = "translate(0,0)" ;
        document.getElementById(img_icon).style.margin = "0 0 0 0";
        element.style.display = "none";
    });

    var box=document.getElementById("mydivMA");
    if(box.style.display == "none")
    {
      document.getElementById("Ltab_headerMA").style.transform = "translate(0,45%)";
      document.getElementById("img_iconMA").style.margin = "30px 0 0 0";
    //   document.getElementById("caretMA").style.marginTop = "17px";
      box.style.display = "block" ;
    }
    else
    {   
        document.getElementById("Ltab_headerMA").style.transform = "translate(0,0)";
        document.getElementById("img_iconMA").style.margin = "0 0 0 0";
        // document.getElementById("caretMA").style.margin = "0";
        box.style.display = "none";
    }

});


// 3rd Tab ST


document.getElementById("mybtnST").addEventListener("click",function(event){
  
    var a =document.getElementsByClassName("click_me");

    Array.from(a).forEach(element => {
        var img_icon = "img_icon"+ element.id.slice(-(element.id.length-5));
        var Ltab_header = "Ltab_header" + element.id.slice(-(element.id.length-5));
        // console.log(Ltab_header)
        document.getElementById(Ltab_header).style.transform = "translate(0,0)" ;
        // var Ltab_caret = "caret" + element.id.slice(-(element.id.length-5));
        // document.getElementById(Ltab_caret).style.marginTop = "0";
        document.getElementById(img_icon).style.margin = "0 0 0 0";
        element.style.display = "none";
    });

    var box=document.getElementById("mydivST");
    if(box.style.display == "none")
    {
      document.getElementById("Ltab_headerST").style.transform = "translate(0,45%)";
      document.getElementById("img_iconST").style.margin = "30px 0 0 0";
    //   document.getElementById("caretST").style.marginTop = "17px";
        box.style.display = "block" ;
    }
    else
    {
        document.getElementById("Ltab_headerST").style.transform = "translate(0,0)";
        document.getElementById("img_iconST").style.margin = "0 0 0 0";
        // document.getElementById("caretST").style.marginTop = "0";
        box.style.display = "none";
    }

});
// 4th Tab


document.getElementById("mybtnHO").addEventListener("click",function(event){
  
    var a =document.getElementsByClassName("click_me");

    Array.from(a).forEach(element => {
        var img_icon = "img_icon"+ element.id.slice(-(element.id.length-5));
        var Ltab_header = "Ltab_header" + element.id.slice(-(element.id.length-5));
        document.getElementById(Ltab_header).style.transform = "translate(0,0)";
        // var Ltab_caret = "caret" + element.id.slice(-(element.id.length-5));
        // document.getElementById(Ltab_caret).style.marginTop = "0";
        document.getElementById(img_icon).style.margin = "0 0 0 0";
        element.style.display = "none";
    });

    var box=document.getElementById("mydivHO");
    if(box.style.display == "none")
    {
      document.getElementById("Ltab_headerHO").style.transform = "translate(0,45%)";
      document.getElementById("img_iconHO").style.margin = "30px 0 0 0";
    //   document.getElementById("caretHO").style.marginTop = "17px";
        box.style.display = "block" ;
    }
    else
    {  
         document.getElementById("Ltab_headerHO").style.transform = "translate(0,0)";
        document.getElementById("img_iconHO").style.margin = "0 0 0 0";
        // document.getElementById("caretHO").style.marginTop = "0";
        box.style.display = "none";
    }

});

// Click RTAB new from card1 class
var m = document.getElementsByClassName("card1")
// console.log(m);
Array.from(m).forEach(car => {
    Array.from(car.children).forEach(beta => {
        // console.log(beta.id);   //mybtnTR, mybtnCSI...
        document.getElementById(beta.id).addEventListener("click",function(event){  
            sublist_items = document.getElementsByClassName('sublist');
                // console.log(item.id);  mydivTR mydivCSI etc,  beta.id.length-5,beta.id.length
                var item = "mydiv"+ beta.id.slice(-(beta.id.length-5));
                // console.log(item); //mydivTR

                var Rtab_icon1 = "icon" + beta.id.slice(-(beta.id.length-5));
                // console.log(Rtab_icon1);
                var Rtab_content1 = "content" + beta.id.slice(-(beta.id.length-5));
                // console.log(Rtab_content1);
                var Rtab_header1 = "Rtab_header" + beta.id.slice(-(beta.id.length-5));
                //  console.log(Rtab_header1);
             
                var box_id1 = document.getElementById(item).style.display;
                // console.log(box_id1);
                var box_id = document.getElementById(item);
    // for opening one at a time TR or CSI or others
               if(box_id1!="none" && box_id1!="block")
               {
                   box_id1="none";
               }
                Array.from(sublist_items).forEach(items => {
                    var z = "Rtab_header" + items.id.slice(-(items.id.length-5));
                    var y = "icon" + items.id.slice(-(items.id.length-5));
                    var x = "content" + items.id.slice(-(items.id.length-5)); 
                    
                    // console.log(x);
                    // console.log(document.getElementById(x));
                    document.getElementById(x).style.marginTop = "5px";
                    document.getElementById(y).style.marginTop = "5px";
                    document.getElementById(z).style.marginTop = "14px";
                    items.style.display = "none";
                });

             //tab opening one at a time ends
                // var box_id = document.getElementById(item);,l
                // console.log(box_id);
                
                if(box_id1 == "none")
                {               
                    box_id.style.display = "block" ;   
                    document.getElementById(Rtab_icon1).style.marginTop = "12px";
                    document.getElementById(Rtab_content1).style.marginTop = "22px";
                    document.getElementById(Rtab_header1).style.marginTop = "1px"; 
                }
                else
                {
                    document.getElementById(Rtab_icon1).style.marginTop = "5px";
                    document.getElementById(Rtab_content1).style.marginTop = "5px"; 
                    document.getElementById(Rtab_header1).style.marginTop = "14px";
                    box_id.style.display = "none";  
                }
                // console.log(box_id.style.display)
        });
    }); 
});


// popovers initialization - on hover
$('[data-toggle="popover-hover"]').popover({
    html: true,
    trigger: 'hover',
    placement: 'right',
    content: function () { return '<img class="img-fluid" width= "150px" src="' + $(this).data('img') + '" />'; }
  });
// width= "150px" can be changed

// $(document).ready(function(){
//     $('#popoverAM').popover({
//         content : '<div class="img-fluid media"><img src="img/analytics.png" width="100%" alt="Sample Image"></div>'
//     });

// });


document.getElementById("year").innerHTML = new Date().getFullYear();


// in click :id= mybtn    jo display :div id="mydiv"


